#!/bin/bash
# https://github.com/taw00/howto/blob/master/howto-configure-firewalld.md
# https://computingforgeeks.com/how-to-install-and-configure-firewalld-on-debian/

sudo apt-get install firewalld
sudo systemctl enable firewalld.service
sudo systemctl start firewalld

sudo firewall-cmd --add-masquerade --permanent
sudo firewall-cmd --add-service={ssh,http,https} --permanent

sudo firewall-cmd --zone=public --permanent --add-rich-rule='rule service name=ssh accept limit value=10/m'
sudo firewall-cmd --zone=public --permanent --add-rich-rule='rule service name=http accept limit value=10/s'
sudo firewall-cmd --zone=public --permanent --add-rich-rule='rule service name=https accept limit value=10/s'

sudo firewall-cmd --permanent --set-target=REJECT

sudo firewall-cmd --permanent --zone=trusted --add-port=4243/tcp

# sudo firewall-cmd --zone=public --add-port={8000,9000}/tcp --permanent

sudo firewall-cmd --reload
sudo firewall-cmd --list-all
sudo iptables -L -v -n | more
