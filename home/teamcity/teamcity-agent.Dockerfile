FROM jetbrains/teamcity-agent

USER root

RUN apt-get update && \
    apt-get -y upgrade  && \
    apt-get -y install curl gnupg2 dirmngr apt-transport-https lsb-release ca-certificates

# install nodejs and chromium browser for tests
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get -y install nodejs chromium-browser

# install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update  && \
    apt-get -y install yarn


# update to latest docker
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
    apt-get update && \
    apt-get -y install docker-ce docker-ce-cli containerd.io

# update to latest docker-compose
RUN apt-get -y install python3-pip && \
    pip3 install docker-compose

# docker registry login support
RUN apt-get -y install pass

USER buildagent
