find /home/webproxy -name "*.sh" -execdir chmod u+x {} +
find /home/portainer -name "*.sh" -execdir chmod u+x {} +
cd ./webproxy/bin
./fresh-start.sh -e "christian.kusmanow@teslasoft.de" -d "/home/webproxy/data" -net "webproxy" \
  --use-nginx-conf-files --update-nginx-template --activate-ipv6
