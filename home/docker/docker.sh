#!/bin/bash
# https://www.kali.org/docs/containers/installing-docker-on-kali/
# https://docs.docker.com/network/iptables/

printf "%s\n" "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-ce-archive-keyring.gpg] https://download.docker.com/linux/debian buster stable" \
  | sudo tee /etc/apt/sources.list.d/docker-ce.list

curl -fsSL https://download.docker.com/linux/debian/gpg \
  | gpg --dearmor \
  | sudo tee /usr/share/keyrings/docker-ce-archive-keyring.gpg >/dev/null

sudo apt-get update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose

sudo usermod -aG docker $USER
